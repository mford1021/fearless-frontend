function createCard(name, description, pictureURL, starts, ends) {
  return `
    <div class="card-group row-gap-3">
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureURL}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${starts} ${description}</p>
        </div>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureURL = details.conference.location.picture_url;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const html = createCard(name, description, pictureURL, starts, ends);

          const column = document.querySelector('.row-cols-3');
          column.innerHTML += html;
        }
      }

    }
  } catch (e) {
    // Figure out what to do if an error is raised
  }

});

//window.addEventListener("click", e => 
//)
